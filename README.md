# swift

simple docker images with swift installed. See the container registry for more.

## Note

swift versions < 5.2.4 are no longer supported cause of ubuntu 20.04 base image.

## swiftenv image

The swiftenv image is based on [Kylef](https://github.com/kylef)'s work so all the credits go to him. The little changes are base image from ubuntu 16.04 to ubuntu 20.04 and swiftenv version from 1.2.1 to 1.4.0.
