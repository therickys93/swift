import XCTest
@testable import CalculatorSDK

final class CalculatorSDKTests: XCTestCase {

    func testAddition() {
        let a = 5
        let b = 6
        let c = Calculator.add(a: a, b: b)
        XCTAssertEqual(11, c)
    }

    func testSubtraction() {
        let a = 5
        let b = 6
        let c = Calculator.sub(a: a, b: b)
        XCTAssertEqual(-1, c)
    }

    func testDefaultSubtraction() {
        let a = 5
        let b = 6
        let c = Calculator.default_sub(a: a, b: b)
        XCTAssertEqual(-1, c)
    }

    func testMultiplication() {
        let a = 5
        let b = 6
        let c = Calculator.mul(a: a, b: b)
        XCTAssertEqual(30, c)
    }

    func testDefaultMultiplication() {
        let a = 5
        let b = 6
        let c = Calculator.default_mul(a: a, b: b)
        XCTAssertEqual(30, c)
    }

    func testDivision() {
        let a = 5
        let b = 6
        let c = Calculator.div(a: a, b: b)
        XCTAssertEqual(0, c)
        let d = Calculator.div(a: b, b: a)
        XCTAssertEqual(1, d)
    }

    func testDefaultDivision() {
        let a = 5
        let b = 6
        let c = Calculator.default_div(a: a, b: b)
        XCTAssertEqual(0, c)
        let d = Calculator.default_div(a: b, b: a)
        XCTAssertEqual(1, d)
    }
    
    static var allTests = [
        ("testAddition", testAddition),
        ("testSubtraction", testSubtraction),
        ("testDefaultSubtraction", testDefaultSubtraction),
        ("testMultiplication", testMultiplication),
        ("testDefaultMultiplication", testDefaultMultiplication),
        ("testDivision", testDivision),
        ("testDefaultDivision", testDefaultDivision),
    ]
}
